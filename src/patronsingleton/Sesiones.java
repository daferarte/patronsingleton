/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patronsingleton;

/**
 *
 * @author dafer
 */
public class Sesiones {

    private static Sesiones instancia; //una variable del mismo tipo de la clase privada que se pueda reutilizar
    
    private Sesiones() {
        System.out.println("Cree una clase");
    }
    
    //crear un metodo del patron singleton para devolver la instancia de la clase 
    public static Sesiones getInstance(){
        if(instancia==null){
            instancia=new Sesiones();
        }
        return instancia;
    }
    
    
}
